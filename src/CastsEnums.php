<?php

declare(strict_types=1);

namespace Jivix\LaravelEnum;

use ReflectionClass;
use ReflectionException;
use RuntimeException;

trait CastsEnums
{
    /**
     * Overrides laravel's castAttribute method.
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        $class = $this->getClassName($key);

        if (null === $class || null === $value) {
            return parent::castAttribute($key, $value);
        }

        /** @var Enum $class */
        return $class::fromNative($value);
    }

    /**
     * Overrides laravel's setAttribute method.
     *
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function setAttribute(string $key, $value)
    {
        $class = $this->getClassName($key);

        if (null === $class || null === $value || !$value instanceof $class) {
            return parent::setAttribute($key, $value);
        }

        /** @var Enum $value */
        return parent::setAttribute($key, $value->getNativeValue());
    }

    /**
     * Returns Class name.
     *
     * @param $key
     * @return null
     */
    private function getClassName($key)
    {
        try {
            $casts = $this->getCasts();
            $class = isset($casts[$key]) ? $casts[$key] : null;
            if (class_exists($class)) {
                $reflection = new ReflectionClass($class);

                if ($reflection->implementsInterface('App\Common\Enum\Castable')) {
                    return $class;
                }
            }
            return null;
        } catch (ReflectionException $e) {
            throw new RuntimeException($e->getMessage());
        }
    }

    abstract public function getCasts();
}